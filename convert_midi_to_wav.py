import glob
import os
import mido
import json
import statistics
import time
from pydub import AudioSegment
from audiolazy import lazy_midi
from midi2audio import FluidSynth
import multiprocessing

script_dir = os.path.dirname(os.path.realpath(__file__))
midi_dir = os.path.join(script_dir, "midi") # , "mozart", "kunstderfuge", "sonatas", "no_sections"
wav_dir = os.path.join(script_dir, "wav")

def convert_secs_to_hms(secs):
    return time.strftime('%H:%M:%S', time.gmtime(secs))

def parse_midi_to_json(midi_fn):
    midi_data = {}
    try:
        mid = mido.MidiFile(midi_fn)

        try:
            midi_data["length_in_seconds"] = mid.length
        except:
            print("WARNING: can't find length in %s" % midi_fn)
            midi_data["length_in_seconds"] = None
        
        midi_data["ticks_per_beat"] = mid.ticks_per_beat
        midi_data["tracks"] = {}

        last_msg = None
        current_seconds = 0
        current_tempo = 0

        for i, track in enumerate(mid.tracks):
            #print('Track {}: {}'.format(i, track.name))
            track_data = midi_data["tracks"].setdefault(track.name, {})
            track_meta_data = track_data.setdefault("metadata", [])
            track_roll = track_data.setdefault("roll", [])
            track_tempos = track_data.setdefault("tempos", [])
            track_tempo_stats = track_data.setdefault("tempo_stats", {})
            for msg in track:
                if msg.is_meta:
                    meta_entry_dict = msg.dict()
                    meta_name = meta_entry_dict["type"]

                    if meta_name == "set_tempo":
                        meta_entry_dict["converted_bpm"] = mido.bpm2tempo(meta_entry_dict["tempo"])
                        track_tempos.append({"tempo": meta_entry_dict["tempo"], "bpm": meta_entry_dict["converted_bpm"], "timestamp_secs": current_seconds, "timestamp_hms": convert_secs_to_hms(current_seconds)})
                        current_tempo = meta_entry_dict["tempo"]

                    # Attempt to timestamp
                    meta_entry_dict["timestamp_secs"] = current_seconds
                    meta_entry_dict["timestamp_hms"] = convert_secs_to_hms(current_seconds)

                    track_meta_data.append(meta_entry_dict)
                else:
                    cmd_msg = msg.dict()

                    if cmd_msg["type"] == "note_on":
                        # Provide conversions to frequency and note name
                        cmd_msg["note_name"] = lazy_midi.midi2str(cmd_msg["note"])
                        cmd_msg["note_freq"] = lazy_midi.midi2freq(cmd_msg["note"])

                    # Attempt to timestamp
                    cmd_msg["timestamp_secs"] = current_seconds
                    cmd_msg["timestamp_hms"] = convert_secs_to_hms(current_seconds)

                    track_roll.append(cmd_msg)
                
                if "time" in msg.dict():
                    current_seconds += mido.tick2second(msg.time, midi_data["ticks_per_beat"], current_tempo)
                last_msg = msg.dict()
            
            # Compute tempo stats (if tempos are available)
            if track_tempos:
                track_tempo_stats["max_tempo_entry"] = max(track_tempos, key=lambda t: t["bpm"])
                track_tempo_stats["min_tempo_entry"] = min(track_tempos, key=lambda t: t["bpm"])
                track_tempo_stats["avg_bpm"] = sum([t["bpm"] for t in track_tempos]) / (0.0 + len(track_tempos))
                track_tempo_stats["avg_tempo"] = sum([t["tempo"] for t in track_tempos]) / (0.0 + len(track_tempos))
                track_tempo_stats["med_bpm"] = statistics.median([t["bpm"] for t in track_tempos])
                track_tempo_stats["med_tempo"] = statistics.median([t["tempo"] for t in track_tempos])
    except EOFError:
        print("WARNING: error encountered for midi parse (EOFError), data may be incomplete: %s" % midi_fn)
        pass
    except mido.midifiles.meta.KeySignatureError:
        print("WARNING: error encountered for midi parse (KeySignatureError), data may be incomplete: %s" % midi_fn)
        pass
    except OSError:
        print("WARNING: error encountered for midi parse (OSError), data may be incomplete: %s" % midi_fn)

    return midi_data

def get_extra_metadata(midi_dir):
    index_json_fn = os.path.join(midi_dir, "index.json")

    print("searching for extra metadata: %s" % index_json_fn)

    meta_obj = {}

    if os.path.isfile(index_json_fn):
        with open(index_json_fn, "r") as fh:
            try:
                meta_obj = json.loads(fh.read())
            except ValueError:
                print("WARNING: metadata file failed to load: %s" % index_json_fn)
    else:
        print("WARNING: no metadata found: %s" % index_json_fn)
    
    return meta_obj

def interpret_section_times_to_ms(start_time, end_time):
    if start_time == "START":
        start_time = 0
    else:
        # mm:ss
        start_time_sp = start_time.split(":")
        assert len(start_time_sp) == 2
        start_time_mins = float(start_time_sp[0])
        start_time_secs = float(start_time_sp[1])
        start_time = (60 * 1000 * start_time_mins) + (1000 * start_time_secs)
    
    if end_time == "END":
        end_time = None
    else:
        # mm:ss
        end_time_sp = end_time.split(":")
        assert len(end_time_sp) == 2
        end_time_mins = float(end_time_sp[0])
        end_time_secs = float(end_time_sp[1])
        end_time = (60 * 1000 * end_time_mins) + (1000 * end_time_secs)
    
    return start_time, end_time

def slice_audio_by_sections(wav_file, sections):
    # all time is indexed by milliseconds
    wav = AudioSegment.from_wav(wav_file)
    section_ctr = 1

    print("slicing wav_file=%s" % wav_file)

    for section in sections:
        # Parse start and end time
        start_time = section["start"]
        end_time = section["end"]
        speed = section.get("speed", "unknown")

        start_time, end_time = interpret_section_times_to_ms(start_time, end_time)
        
        print("performing wav split: section %d (speed %s) from %s (%s secs) to %s (%s secs)" %
              (section_ctr, speed, section["start"], start_time, section["end"], end_time))

        new_wave_file = os.path.splitext(wav_file)[0]
        new_wave_file += (".section_%d" % section_ctr)
        new_wave_file += ".speed_" + speed
        new_wave_file += ".from_" + section["start"] + "_to_" + section["end"] + ".wav"
        
        print("  saving as: %s" % new_wave_file)

        wav[slice(start_time, end_time)].export(new_wave_file, format="wav")

        section_ctr += 1

def compute_stats_for_tempo_in_sections(midi_data, sections):
    tempo_list = []
    sections_tempo_stats = []
    for track, track_data in midi_data["tracks"].items():
        if "tempos" in track_data and track_data["tempos"]:
            tempo_list += track_data["tempos"]
    
    for section in sections:
        start_time = section["start"]
        end_time = section["end"]

        start_time, end_time = interpret_section_times_to_ms(start_time, end_time)
        
        # Convert back to seconds
        start_time /= 1000
        if end_time:
            end_time /= 1000

        filtered_tempo_list = [tempo for tempo in tempo_list
                               if start_time <= tempo["timestamp_secs"] and
                               ((end_time and tempo["timestamp_secs"] <= end_time) or (not end_time))]

        # Stats time
        section_tempo_stats = {}
        section_tempo_stats["max_tempo_entry"] = max(filtered_tempo_list, key=lambda t: t["bpm"])
        section_tempo_stats["min_tempo_entry"] = min(filtered_tempo_list, key=lambda t: t["bpm"])
        section_tempo_stats["avg_bpm"] = sum([t["bpm"] for t in filtered_tempo_list]) / (0.0 + len(filtered_tempo_list))
        section_tempo_stats["avg_tempo"] = sum([t["tempo"] for t in filtered_tempo_list]) / (0.0 + len(filtered_tempo_list))
        section_tempo_stats["med_bpm"] = statistics.median([t["bpm"] for t in filtered_tempo_list])
        section_tempo_stats["med_tempo"] = statistics.median([t["tempo"] for t in filtered_tempo_list])
        sections_tempo_stats.append(section_tempo_stats)
    
    return sections_tempo_stats

def process_midi_file(root, midi_file):
    fs = FluidSynth()

    midi_fn = os.path.join(root, midi_file)
    wav_fn = midi_fn.replace(midi_dir, wav_dir)
    wav_fn = os.path.splitext(wav_fn)[0] + '.wav'
    specific_wav_dir = os.path.dirname(wav_fn)
    print("creating dir: %s" % os.path.dirname(wav_fn))
    os.makedirs(os.path.dirname(wav_fn), exist_ok=True)
    print("converting: %s to %s" % (midi_fn, wav_fn))
    #fs.midi_to_audio(midi_fn, wav_fn)
    print("parsing metadata: %s" % midi_fn)
    midi_data = parse_midi_to_json(midi_fn)
    extra_metadata = get_extra_metadata(root)
    final_metadata = dict(**extra_metadata)
    if "files" in final_metadata:
        final_metadata.pop("files")
    
    if midi_file in extra_metadata["files"]:
        for k, v in extra_metadata["files"][midi_file].items():
            final_metadata[k] = v
    
    if "sections" in final_metadata:
        print("found sections definition!")
        if midi_data:
            print(" -> computing stats on tempo for sections...")
            sections_tempo_stats = compute_stats_for_tempo_in_sections(midi_data, final_metadata["sections"])
            for section, section_tempo_stats in zip(final_metadata["sections"], sections_tempo_stats):
                section["tempo_stats"] = section_tempo_stats
        else:
            print(" -> skipping computing stats on tempo for sections (no midi data available)")
        print(" -> computing section duration...")
        for section in final_metadata["sections"]:
            start_time, end_time = interpret_section_times_to_ms(section["start"], section["end"])
            if end_time is None:
                # assume that it's the midi duration
                if not midi_data:
                    print("    -> could not find midi data, skipping duration computation for last entry")
                    continue
                end_time = midi_data["length_in_seconds"] * 1000.0
            section["duration"] = (end_time - start_time) / 1000.0
        print(" -> performing slices...")
        slice_audio_by_sections(wav_fn, final_metadata["sections"])

    if midi_data and "length_in_seconds" in midi_data:
        final_metadata["duration"] = midi_data["length_in_seconds"]

    final_metadata["midi"] = midi_data

    md_path = os.path.join(specific_wav_dir, "%s.metadata.json" % midi_file)

    print("writing metadata: %s" % md_path)

    with open(md_path, "w") as md_fh:
        md_fh.write(json.dumps(final_metadata, indent=4))

def process_midi_file_async(root, midi_file):
    try:
        process_midi_file(root, midi_file)
    except:
        import traceback
        traceback.print_exc()

if __name__ == "__main__":
    pool = multiprocessing.Pool(processes=10)
    delayed = []
    for root, subdirs, midi_files in os.walk(midi_dir):
        if midi_files:
            for midi_file in midi_files:
                if midi_file.endswith(".mid"):
                    #process_midi_file(midi_file)
                    delayed.append(pool.apply_async(process_midi_file_async, (root, midi_file,)))
                    #process_midi_file_async(root, midi_file)
    queue_ct = 0
    for d in delayed:
        d.wait()
        queue_ct += 1
        print("\033[1m ** completed: %d/%d (%.2f%%) ** \033[0m" % (queue_ct, len(delayed), (100.0 * queue_ct)/len(delayed)))
    pool.close()
    pool.join()
