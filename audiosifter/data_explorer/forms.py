from django import forms
from django_select2.forms import Select2MultipleWidget
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Row, Column

from music_library import _load_json_files

json_cache = _load_json_files()

class SearchForm(forms.Form):
    author = forms.MultipleChoiceField(label="Author", required=False,
        choices=((k, k) for k in json_cache["index"]["author"].keys()),
        widget=Select2MultipleWidget(attrs={"data-allow-clear": "true"}))
    category = forms.MultipleChoiceField(label="Category", required=False,
        choices=((k, k) for k in json_cache["index"]["category"].keys()),
        widget=Select2MultipleWidget(attrs={"data-allow-clear": "true"}))
    name = forms.MultipleChoiceField(label="Name", required=False,
        widget=Select2MultipleWidget(attrs={"data-tags": "true", "data-multiple": "multiple", "data-placeholder": "Enter regex and hit ENTER to add.", "data-allow-clear": "true"}))
    from_year = forms.CharField(label='From year:', required=False, max_length=4)
    to_year = forms.CharField(label='To year:', required=False, max_length=4)
    from_movement = forms.CharField(label='From movement:', required=False, max_length=4)
    to_movement = forms.CharField(label='To movement:', required=False, max_length=4)
    key = forms.MultipleChoiceField(label="Key", required=False,
        choices=((k, k) for k in json_cache["index"]["key"].keys()),
        widget=Select2MultipleWidget(attrs={"data-allow-clear": "true"}))
    speed = forms.MultipleChoiceField(label="Speed", required=False,
        choices=((k, k) for k in json_cache["index"]["speed"].keys()),
        widget=Select2MultipleWidget(attrs={"data-tags": "true", "data-multiple": "multiple", "data-placeholder": "Enter regex and hit ENTER to add.", "data-allow-clear": "true"}))
    from_duration = forms.CharField(label='From duration:', required=False, max_length=4)
    to_duration = forms.CharField(label='To duration:', required=False, max_length=4)
    from_min_bpm = forms.CharField(label='From min BPM:', required=False, max_length=4)
    to_min_bpm = forms.CharField(label='To min BPM:', required=False, max_length=4)
    from_max_bpm = forms.CharField(label='From max BPM:', required=False, max_length=4)
    to_max_bpm = forms.CharField(label='To max BPM:', required=False, max_length=4)
    from_avg_bpm = forms.CharField(label='From avg BPM:', required=False, max_length=4)
    to_avg_bpm = forms.CharField(label='To avg BPM:', required=False, max_length=4)
    from_med_bpm = forms.CharField(label='From med BPM:', required=False, max_length=4)
    to_med_bpm = forms.CharField(label='To med BPM:', required=False, max_length=4)


    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Row(
                Column('author', css_class='form-group col-md-4 mb-0'),
                Column('category', css_class='form-group col-md-4 mb-0'),
                Column('name', css_class='form-group col-md-4 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('from_year', css_class='form-group col-md-3 mb-0'),
                Column('to_year', css_class='form-group col-md-3 mb-0'),
                Column('from_movement', css_class='form-group col-md-3 mb-0'),
                Column('to_movement', css_class='form-group col-md-3 mb-0'),
            ),
            Row(
                Column('key', css_class='form-group col-md-6 mb-0'),
                Column('speed', css_class='form-group col-md-6 mb-0'),
            ),
            Row(
                Column('from_duration', css_class='form-group col-md-6 mb-0'),
                Column('to_duration', css_class='form-group col-md-6 mb-0')
            ),
            Row(
                Column('from_min_bpm', css_class='form-group col-md-3 mb-0'),
                Column('to_min_bpm', css_class='form-group col-md-3 mb-0'),
                Column('from_max_bpm', css_class='form-group col-md-3 mb-0'),
                Column('to_max_bpm', css_class='form-group col-md-3 mb-0'),
            ),
            Row(
                Column('from_avg_bpm', css_class='form-group col-md-3 mb-0'),
                Column('to_avg_bpm', css_class='form-group col-md-3 mb-0'),
                Column('from_med_bpm', css_class='form-group col-md-3 mb-0'),
                Column('to_med_bpm', css_class='form-group col-md-3 mb-0'),
            ),
            Submit('submit', 'Search')
        )