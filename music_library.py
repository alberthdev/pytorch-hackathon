#!/usr/bin/env python3
# Music Library... library
# Given specific criteria, get the audio files that match said criteria.
import copy
import json
import logging
import os
import re
import typing

from collections import namedtuple

MusicEntry = namedtuple("MusicEntry", "wav_file json_file summary")
MusicEntrySectionSummary = namedtuple("MusicEntrySectionSummar", "section_num start end")
MusicEntrySummary = namedtuple("MusicEntrySummary", "author category name year movement key speed duration min_bpm max_bpm avg_bpm med_bpm tempo_stats is_section section")

script_dir = os.path.dirname(os.path.realpath(__file__))
wav_dir = os.path.join(script_dir, "wav")

# This attempts to load key information about each wav file
# and provide a quick index.
def _load_json_files() -> typing.Dict[str, typing.Any]:
    # Grab key attributes
    author_to_entry: typing.Dict[typing.Optional[str], typing.List[MusicEntry]] = {}
    category_to_entry: typing.Dict[typing.Optional[str], typing.List[MusicEntry]] = {}
    name_to_entry: typing.Dict[typing.Optional[str], typing.List[MusicEntry]] = {}
    year_to_entry: typing.Dict[typing.Optional[str], typing.List[MusicEntry]] = {}
    movement_to_entry: typing.Dict[typing.Optional[str], typing.List[MusicEntry]] = {}
    key_to_entry: typing.Dict[typing.Optional[str], typing.List[MusicEntry]] = {}
    speed_to_entry: typing.Dict[typing.Optional[str], typing.List[MusicEntry]]= {}
    duration_to_entry: typing.Dict[typing.Optional[str], typing.List[MusicEntry]] = {}
    min_bpm_to_entry: typing.Dict[typing.Optional[str], typing.List[MusicEntry]] = {}
    max_bpm_to_entry: typing.Dict[typing.Optional[str], typing.List[MusicEntry]] = {}
    avg_bpm_to_entry: typing.Dict[typing.Optional[str], typing.List[MusicEntry]] = {}
    med_bpm_to_entry: typing.Dict[typing.Optional[str], typing.List[MusicEntry]] = {}

    wav_files = set()
    json_file_to_wav_file: typing.Dict[str, typing.Set[str]] = {}
    wav_file_to_music_entry: typing.Dict[str, MusicEntry] = {}
    wav_file_to_tldr_entry: typing.Dict[str, typing.Dict[str, typing.Any]] = {}

    for root, subdirs, json_files in os.walk(wav_dir):
        if not json_files:
            continue
        for json_file in json_files:
            if not json_file.endswith(".metadata.json"):
                continue
            json_file = os.path.join(root, json_file)
            wav_file = json_file.replace(".mid.metadata.json", ".wav")

            if json_file in json_file_to_wav_file:
                logging.warn("multiple json files detected: %s", json_file)
            
            assoc_wav_file_set = json_file_to_wav_file.setdefault(json_file, set())
            
            with open(json_file, "r") as fh:
                obj = json.load(fh)

                file_author = obj.get("author")
                file_category = obj.get("category")
                file_name = obj.get("name")
                file_year = obj.get("year")
                file_movement = obj.get("movement")
                file_key = obj.get("key")
                file_speed = obj.get("speed")
                file_duration = obj.get("duration")
                file_min_bpm = None
                file_max_bpm = None
                file_avg_bpm = None
                file_med_bpm = None
                tempo_stats = None

                # Try and find tempo stats, if available
                if "midi" in obj and obj["midi"] and "tracks" in obj["midi"]:
                    for track_title, track in obj["midi"]["tracks"].items():
                        if track.get("tempo_stats", {}):
                            tempo_stats = track["tempo_stats"]
                            file_min_bpm = tempo_stats["min_tempo_entry"]["bpm"]
                            file_max_bpm = tempo_stats["max_tempo_entry"]["bpm"]
                            file_avg_bpm = tempo_stats["avg_bpm"]
                            file_med_bpm = tempo_stats["med_bpm"]

                # Section detection
                if "sections" in obj:
                    section_ctr = 1
                    for section in obj["sections"]:
                        author = section.get("author") or file_author
                        category = section.get("category") or file_category
                        name = section.get("name") or file_name
                        year = section.get("year") or file_year
                        movement = section.get("movement") or file_movement
                        key = section.get("key") or file_key
                        speed = section.get("speed") or file_speed
                        duration = section.get("duration") or file_duration
                        min_bpm = file_min_bpm
                        max_bpm = file_max_bpm
                        avg_bpm = file_avg_bpm
                        med_bpm = file_med_bpm
                        tempo_stats = None

                        # Try and find tempo stats, if available
                        if section.get("tempo_stats", {}):
                            tempo_stats = section["tempo_stats"]
                            min_bpm = tempo_stats["min_tempo_entry"]["bpm"]
                            max_bpm = tempo_stats["max_tempo_entry"]["bpm"]
                            avg_bpm = tempo_stats["avg_bpm"]
                            med_bpm = tempo_stats["med_bpm"]

                        new_wave_file = os.path.splitext(wav_file)[0]
                        new_wave_file += (".section_%d" % section_ctr)
                        new_wave_file += ".speed_" + speed
                        new_wave_file += ".from_" + section["start"] + "_to_" + section["end"] + ".wav"

                        wav_file_to_tldr_entry[new_wave_file] = MusicEntrySummary(
                            author=author, category=category, name=name, year=year, movement=movement, key=key, speed=speed, duration=duration,
                            min_bpm=min_bpm, max_bpm=max_bpm, avg_bpm=avg_bpm, med_bpm=med_bpm,
                            tempo_stats=json.dumps(tempo_stats) if tempo_stats else None,
                            is_section=True,
                            section=MusicEntrySectionSummary(section_num=section_ctr, start=section["start"], end=section["end"]))

                        music_entry = MusicEntry(wav_file=new_wave_file, json_file=json_file, summary=copy.deepcopy(wav_file_to_tldr_entry[new_wave_file]))

                        # TODO: should we not add the regular length wav file?
                        wav_files.add(new_wave_file)
                        assoc_wav_file_set.add(new_wave_file)
                        wav_file_to_music_entry[new_wave_file] = music_entry

                        # Save data
                        author_to_entry.setdefault(author, []).append(music_entry)
                        category_to_entry.setdefault(category, []).append(music_entry)
                        name_to_entry.setdefault(name, []).append(music_entry)
                        year_to_entry.setdefault(year, []).append(music_entry)
                        movement_to_entry.setdefault(movement, []).append(music_entry)
                        key_to_entry.setdefault(key, []).append(music_entry)
                        speed_to_entry.setdefault(speed, []).append(music_entry)
                        duration_to_entry.setdefault(duration, []).append(music_entry)
                        min_bpm_to_entry.setdefault(min_bpm, []).append(music_entry)
                        max_bpm_to_entry.setdefault(max_bpm, []).append(music_entry)
                        avg_bpm_to_entry.setdefault(avg_bpm, []).append(music_entry)

                        section_ctr += 1
                else:
                    wav_file_to_tldr_entry[new_wave_file] = MusicEntrySummary(
                            author=file_author, category=file_category, name=file_name, year=file_year, movement=file_movement,
                            key=file_key, speed=file_speed, duration=file_duration,
                            min_bpm=file_min_bpm, max_bpm=file_max_bpm, avg_bpm=file_avg_bpm, med_bpm=file_med_bpm,
                            tempo_stats=json.dumps(tempo_stats) if tempo_stats else None,
                            is_section=False,
                            section=None)
                    
                    music_entry = MusicEntry(wav_file=wav_file, json_file=json_file, summary=copy.deepcopy(wav_file_to_tldr_entry[new_wave_file]))

                    wav_files.add(wav_file)
                    assoc_wav_file_set.add(wav_file)
                    wav_file_to_music_entry[wav_file] = music_entry

                    # Save data
                    author_to_entry.setdefault(file_author, []).append(music_entry)
                    category_to_entry.setdefault(file_category, []).append(music_entry)
                    name_to_entry.setdefault(file_name, []).append(music_entry)
                    year_to_entry.setdefault(file_year, []).append(music_entry)
                    movement_to_entry.setdefault(file_movement, []).append(music_entry)
                    key_to_entry.setdefault(file_key, []).append(music_entry)
                    speed_to_entry.setdefault(file_speed, []).append(music_entry)
                    duration_to_entry.setdefault(file_duration, []).append(music_entry)
                    min_bpm_to_entry.setdefault(file_min_bpm, []).append(music_entry)
                    max_bpm_to_entry.setdefault(file_max_bpm, []).append(music_entry)
                    avg_bpm_to_entry.setdefault(file_avg_bpm, []).append(music_entry)
                    med_bpm_to_entry.setdefault(file_med_bpm, []).append(music_entry)

    return {
        "index": {
            "author": author_to_entry,
            "category": category_to_entry,
            "name": name_to_entry,
            "year": year_to_entry,
            "movement": movement_to_entry,
            "key": key_to_entry,
            "speed": speed_to_entry,
            "duration": duration_to_entry,
            "min_bpm": min_bpm_to_entry,
            "max_bpm": max_bpm_to_entry,
            "avg_bpm": avg_bpm_to_entry,
            "med_bpm": med_bpm_to_entry
        },
        "wav_files": wav_files,
        "json_file_to_wav_file": json_file_to_wav_file,
        "wav_file_to_music_entry": wav_file_to_music_entry,
        "wav_file_to_tldr_entry": wav_file_to_tldr_entry
    }

global load_json_files_cache
load_json_files_cache = None

def _memoized_load_json_files(force=False):
    global load_json_files_cache
    if not load_json_files_cache or force:
        load_json_files_cache = _load_json_files()
    return load_json_files_cache

# Get the audio files and associated metadata, given certain criteria.
# 
# All arguments are optional. Arguments left blank will not be used for
# filtering musical pieces.
# 
# With regards to sections, sections are treated as individual pieces.
# 
# Optional arguments:
#     author (set or list of str)
#     category (set or list of str)
#     name (set or list of str, regex)
#     year (range(A,B) int)
#     movement (range(A,B) int)
#     key (set or list of str)
#     speed (set or list of str, regex) (this is the musical "speed", e.g. "allegro")
#     duration (range(A,B) int)
#     min_bpm (range(A,B) float)
#     max_bpm (range(A,B) float)
#     avg_bpm (range(A,B) float)
#     med_bpm (range(A,B) float)
# 
# Returns:
#     tuple(list of str, list of dict)
#         Tuple with list of file paths, and list of associated metadata.
# Definitions:
#     range(A,B) - A through B, excluding B - assuming step=1
# 
# Typing happily enforced through type annotations.
# 
def get_audio_files(author: typing.Optional[typing.Sequence[str]]=None,
                    category: typing.Optional[typing.Sequence[str]]=None,
                    name: typing.Optional[typing.Sequence[str]]=None,
                    year: typing.Optional[range]=None,
                    movement: typing.Optional[range]=None,
                    key: typing.Optional[typing.Sequence[str]]=None,
                    speed: typing.Optional[typing.Sequence[str]]=None,
                    duration: typing.Optional[range]=None,
                    min_bpm: typing.Optional[range]=None,
                    max_bpm: typing.Optional[range]=None,
                    avg_bpm: typing.Optional[range]=None,
                    med_bpm: typing.Optional[range]=None,
                    force_json_reload: typing.Optional[bool]=False) -> typing.Tuple[typing.Set[typing.Any], typing.Set[MusicEntry]]:
    available_data = _memoized_load_json_files(force=force_json_reload)
    
    # split it up
    data_index = available_data["index"]
    wav_files = available_data["wav_files"]
    wav_file_to_music_entry = available_data["wav_file_to_music_entry"]

    # inverted dict
    #wav_file_to_json_file = {v: k for k, v in json_file_to_wav_file.items()}

    # mini checker funcs to help filter this stuff
    # using None as the iterable will result in true
    def _match_exact_in_iter(to_match: str,
                             iterable: typing.Optional[typing.Sequence[str]],
                             search_index: typing.Dict[str, typing.Sequence[MusicEntry]]):
        if not iterable: return True
        if any((search_criteria not in search_index) for search_criteria in iterable):
            missing = [search_criteria for search_criteria in iterable if search_criteria not in search_index]
            raise RuntimeError("could not find search query %s in search_index" % missing)
        
        search_list = [search_value.wav_file for search_criteria in iterable for search_value in search_index[search_criteria]]
        return (to_match in search_list)
    
    def _match_regex_in_iter(to_match: str,
                             iterable: typing.Optional[typing.Sequence[str]],
                             search_index: typing.Dict[str, typing.Sequence[MusicEntry]]):
        if not iterable: return True
        
        iterable_match_from_index = [matched_index_key
                                     for search_criteria in iterable for matched_index_key in search_index
                                     if re.match(search_criteria, matched_index_key)]
        
        search_list = [search_value.wav_file for matched_index_key in iterable_match_from_index for search_value in search_index[matched_index_key]]
        return (to_match in search_list)

    def _match_range(to_match: str,
                     criteria: typing.Optional[range],
                     search_index: typing.Dict[typing.Any, typing.Sequence[MusicEntry]]):
        if not criteria: return True
        start = criteria.start
        stop = criteria.stop

        range_match_from_index = [matched_index_key
                                  for matched_index_key in search_index
                                  if matched_index_key and start <= matched_index_key < stop]
        
        search_list = [search_value.wav_file for matched_index_key in range_match_from_index for search_value in search_index[matched_index_key]]
        return (to_match in search_list)

    matched_wav_files = set()

    for wav_file in wav_files:
        if not _match_exact_in_iter(wav_file, author, data_index["author"]):
            logging.debug("author skip")
            continue
        if not _match_exact_in_iter(wav_file, category, data_index["category"]):
            logging.debug("category skip")
            continue
        if not _match_regex_in_iter(wav_file, name, data_index["name"]):
            logging.debug("name skip")
            continue
        if not _match_range(wav_file, year, data_index["year"]):
            logging.debug("year skip: %s (range: %s)", wav_file, year)
            continue
        if not _match_range(wav_file, movement, data_index["movement"]):
            logging.debug("movement skip")
            continue
        if not _match_exact_in_iter(wav_file, key, data_index["key"]):
            logging.debug("key skip")
            continue
        if not _match_regex_in_iter(wav_file, speed, data_index["speed"]):
            logging.debug("speed skip")
            continue
        if not _match_range(wav_file, duration, data_index["duration"]):
            logging.debug("duration skip")
            continue
        if not _match_range(wav_file, min_bpm, data_index["min_bpm"]):
            logging.debug("min_bpm skip")
            continue
        if not _match_range(wav_file, max_bpm, data_index["max_bpm"]):
            logging.debug("max_bpm skip")
            continue
        if not _match_range(wav_file, avg_bpm, data_index["avg_bpm"]):
            logging.debug("avg_bpm skip")
            continue
        if not _match_range(wav_file, med_bpm, data_index["med_bpm"]):
            logging.debug("med_bpm skip")
            continue
        logging.debug("add: %s" % wav_file)
        matched_wav_files.add(wav_file)
    
    matched_music_entries = set([wav_file_to_music_entry[wav_file] for wav_file in matched_wav_files])

    return matched_wav_files, matched_music_entries

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG,
                        format='[%(asctime)s] [%(levelname)s] %(message)s',
                        )

    matched_wav_files, matched_music_entries = get_audio_files(year=range(1778,1779))
    assert matched_wav_files

    matched_wav_files, matched_music_entries = get_audio_files(author=["Mozart"])
    assert all("mozart" in f for f in matched_wav_files)
    assert all("mozart" in me.wav_file for me in matched_music_entries)

    matched_wav_files, matched_music_entries = get_audio_files(category=["sonatas"])
    assert all("sonata" in f for f in matched_wav_files)
    assert all("sonata" in me.wav_file for me in matched_music_entries)

    matched_wav_files, matched_music_entries = get_audio_files(year=range(1778,1779), author=["Mozart"])
    assert matched_wav_files

    matched_wav_files, matched_music_entries = get_audio_files(duration=range(0,60))
    assert matched_wav_files
    print("0-60s long: %d" % len(matched_wav_files))
    print(matched_wav_files)

    matched_wav_files, matched_music_entries = get_audio_files(avg_bpm=range(0,40))
    assert matched_wav_files
    print("0-40 bpms: %d" % len(matched_wav_files))
    print(sorted(matched_wav_files))

    for matched_music_entry in matched_music_entries:
        print("%s: avg_bpm=%.2f" % (matched_music_entry.wav_file, matched_music_entry.summary.avg_bpm))
    
    matched_wav_files, matched_music_entries = get_audio_files(movement=range(2,3))
    assert matched_wav_files
    print("movement 2: %d" % len(matched_wav_files))
    print(sorted(matched_wav_files))
    assert any(matched_music_entry.summary.is_section for matched_music_entry in matched_music_entries)

    for matched_music_entry in matched_music_entries:
        if matched_music_entry.summary.is_section:
            print("%s: is_section=%s" % (matched_music_entry.wav_file, matched_music_entry.summary.is_section))
    
        